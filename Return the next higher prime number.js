// This challenge is a little bit more complex
// Write a function that takes a number (a) as argument
// If a is prime, return a
// If not, return the next higher prime number
function myFunction(a) {
    for(let i = 2; i <= Math.sqrt(a) ; i++) {
        if(a % i === 0) {
            return myFunction(a + 1)
        }

    }
    return console.log(a);
}
//function myFunction( a ) {
//   function isPrime(num) {
//     for (let i = 2; i <= Math.sqrt(num); i++) {
//       if (num % i === 0) return false;
//     }
//     return num > 1;
//   }
//  let n = a;
//  while (!isPrime(n)) n++;
// return n
// }
myFunction(38);
myFunction(7);
myFunction(115);
myFunction(2000);