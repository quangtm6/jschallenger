// Write a function that takes arguments an arbitrary number of arrays
// It should return an array containing the values of all arrays
function myFunction(...arrays) {
    let x = [];
    for(let i = 0; i < arrays.length; i++) {
        if (Array.isArray(arrays[i])) {
            x.push(...arrays[i])
        }
    }
    return console.log(x);
}
//function myFunction( ...arrays ) {
// return arrays.flat()
// }
myFunction([1, 2, 3], [4, 5, 6]);
myFunction(['a', 'b', 'c'], [4, 5, 6]);
myFunction([true, true], [1, 2], ['a', 'b']);