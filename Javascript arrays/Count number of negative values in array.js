// Write a function that takes an array of numbers as argument
// Return the number of negative values in the array
function myFunction(a) {

    return console.log(a.filter(elem => elem < 0).length);
}

myFunction([1,-2,2,-4]);
myFunction([0,9,1]);
myFunction([4,-3,2,1,0]);