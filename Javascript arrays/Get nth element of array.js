// Write a function that takes an array (a) and a value (n) as argument
// Return the nth element of 'a'
function myFunction(a, n) {

    return console.log(a[n - 1]);
}

myFunction([1,2,3,4,5],3);
myFunction([10,9,8,7,6],5);
myFunction([7,2,1,6,3],1);