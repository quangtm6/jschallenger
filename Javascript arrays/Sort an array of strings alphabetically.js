// Write a function that takes an array of strings as argument
// Sort the array elements alphabetically
// Return the result
function myFunction(arr) {

    return console.log(arr.sort());
}

myFunction(['b', 'c', 'd', 'a']);
myFunction(['z', 'c', 'd', 'a', 'y', 'a', 'w']);