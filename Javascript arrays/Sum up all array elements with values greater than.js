// Write a function that takes an array (a) and a number (b) as arguments
// Sum up all array elements with a value greater than b
// Return the sum
function myFunction(a, b) {
   let x = 0;
   for ( let i = 0; i < a.length; i++) {
       if (a[i] > b) {
           x += a[i];
       }
   }
    return console.log(x);
}
//function myFunction(a, b) {
//   return a.reduce((sum, cur) => {
//     if (cur > b) return sum + cur;
//     return sum;
//   }, 0);
// }
myFunction([1, 2, 3, 4, 5, 6, 7], 2);
myFunction([-10, -11, -3, 1, -4], -3);
myFunction([78, 99, 100, 101, 401], 99);