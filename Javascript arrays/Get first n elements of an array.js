// Write a function that takes an array (a) as argument
// Extract the first 3 elements of a
// Return the resulting array
function myFunction(a) {

    return console.log(a.slice(0, 3));
}

myFunction([1,2,3,4]);
myFunction([5,4,3,2,1,0]);
myFunction([99,1,1]);