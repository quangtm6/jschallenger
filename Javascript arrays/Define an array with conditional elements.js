// Write a function that takes an array with arbitrary elements and a number as arguments
// Return a new array, the first element should be either the given number itself
// or zero if the number is smaller than 6
// The other elements should be the elements of the original array
// Try not to mutate the original array
function
myFunction(arr, num) {
    let x = 0;
    if (num >= 6) {
        x = num;
    }
    arr.unshift(x);
    return console.log(arr);
}
//function myFunction(arr, num) {
//  return [...(num > 5 ? [num] : [0]), ...arr];
// }
myFunction([1,2,3], 6);
myFunction(['a','b'], 2);
myFunction([null,false], 11);