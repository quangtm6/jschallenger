// Write a function that takes an array of objects as argument
// Sort the array by property b in ascending order
// Return the sorted array
function myFunction(arr) {
    let x = 0;
    for(let i = 0; i < arr.length; i++) {
        for(let j= 1; j<arr.length; j++) {
            if (arr[i].b > arr[j].b) {
                x = arr[i];
                arr[i] = arr[j];
                arr[j] = x;
            }
        }
    }
    return console.log(arr);
}
//function myFunction(arr) {
//    const sort = (x, y) => x.b - y.b;
//    return arr.sort(sort);
myFunction([{a:1,b:2},{a:5,b:4}]);
myFunction([{a:2,b:10},{a:5,b:4}]);
myFunction([{a:1,b:7},{a:2,b:1}]);