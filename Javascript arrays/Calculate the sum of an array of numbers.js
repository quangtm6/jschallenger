// Write a function that takes an array of numbers as argument
// It should return the sum of the numbers
//function myFunction(a) {
//     let x = 0;
//     for(let i=0; i< a.length; i++) {
//         x += a[i];
//     }
//     return console.log(x);
// }
function myFunction(a) {
    sum = a.reduce((pre, cur) => pre + cur, 0);
    return console.log(sum);
}

myFunction([10,100,40]);
myFunction([10,100,1000,1]);
myFunction([-50,0,50,200]);