// Write a function that takes two numbers (min and max) as arguments
// Return an array of numbers in the range min to max
function myFunction(min, max) {
    let x = [];
    for (let j = 0; j <= (max - min); j++) {
        x[j] = min;
        min += 1;
        max += 1;
    }
    return console.log(x);
}
//function myFunction(min, max) {
//   let arr = [];
//   for (let i = min; i <= max; i++) {
//     arr.push(i);
//   }
// return arr;
// }
myFunction(2, 10);
myFunction(1, 3);
myFunction(-5, 5);
myFunction(2, 7);