// Write a function that takes an array as argument
// It should return true if all elements in the array are equal
// It should return false otherwise
//function myFunction(arr) {
//     let x = true;
//     for(let i = 1; i < arr.length; i++) {
//         if (arr[i] !== arr[0]) {
//             x = false;
//             break;
//         }
//     }
//     return console.log(x);
//}
function myFunction(arr) {

    return console.log(arr.every(elem => elem === arr[0]));
}
//function myFunction( arr ) {
//   return new Set(arr).size === 1
// }
myFunction([true, true, true, true]);
myFunction(['test', 'test', 'test']);
myFunction([1,1,1,2]);
myFunction(['10',10,10,10]);