// Write a function that takes an array of numbers as argument
// It should return the average of the numbers
function myFunction(arr) {
    return console.log(arr.reduce((pre, cur) => pre + cur, 0)/arr.length);
}

myFunction([10,100,40]);
myFunction([10,100,1000]);
myFunction([-50,0,50,200]);