// Write a function that takes two arrays as arguments
// Merge both arrays and remove duplicate values
// Sort the merge result in ascending order
// Return the resulting array
function myFunction(a, b) {
    for (let i = 0; i < b.length; i++) {
        if (a.includes(b[i]) !== true) {a.unshift(b[i])}
    }
    let x = 0;
    for (let j = 0; j < a.length; j++) {
        for(let k = 0; k < a.length; k++) {
            if (a[j] < a[k]) {
                x = a[j];
                a[j] = a[k];
                a[k] = x;
            }
        }
    }
    return console.log(a);
}
//function myFunction(a, b) {
//   return [...new Set([...a, ...b])].sort((x, y) => x - y);
// }
myFunction([1, 2, 3], [3, 4, 5]);
myFunction([-10, 22, 333, 42], [-11, 5, 22, 41, 42]);
