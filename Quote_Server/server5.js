const express = require('express')
const lodash = require('lodash')
const bodyParser = require('body-parser')
const quotes = require("./quotes.json");
const MongoClient = require('mongodb').MongoClient
const app = express();

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true}))
app.use(express.static('public'))

MongoClient.connect('mongodb://localhost:27017', {useNewUrlParser: true,useUnifiedTopology: true})
    .then(client => {
        console.log('Connected to Database')
        const db = client.db('star-wars-quotes')
        const quotesCollection = db.collection('quotes')

        app.get('/', (req, res) => {
            db.collection(('quotes').find().toArray())
                .then(results => {
                    console.log(results)
                })
                .catch(error => console.error(error))
            res.render('index.ejs', { quotes: results })
        });
        app.get('/quotes', (req, res) => {
            const cursor = db.collection('quotes').find()
        });
        app.post('/quotes', (req,res) => {
            quotesCollection.insertOne(req.body)
                .then(result => {
                    console.log(result)
                    res.redirect('/')
                })
        })

        app.listen(3000, () => {
            console.log("Listening on: " + 3000)
        });
})
    .catch(error => console.error(error))
