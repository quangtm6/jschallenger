const express = require("express");
const app = express();

const quotes = require("./quotes.json");

app.get("/", function(request, response) {
    response.send("Ask me for /quotes");
});

app.get("/quotes", function(request, response) {
    response.json(quotes);
});

const listener = app.listen(process.env.PORT, () =>{
    console.log(listener.address().port);
});