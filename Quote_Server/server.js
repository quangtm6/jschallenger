const express = require('express');
const lodash = require('lodash');
const app = express();
const port = 3000;
const quotes = require('./quotes.json');

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/quotes', (req, res) => {
    res.json(quotes);
});

app.post('/', (req,res) => {
    console.log(req.body);
    quotes.push(req.body);
    res.send('POST Request Called');
    res.redirect('/quotes');
})

app.get('/quotes/:id', (req, res) => {
    const inputId = req.params.id;
    const quote = quotes.filter(res => res.id == inputId)
    res.send(quote);
    })

app.put('/quotes/:id', (req, res) => {
    console.log('Put id')
})
app.get('/quotes/random', (req, res) => {
    res.send(lodash.sample(quotes));
});

app.get('/quotes/search', (req, res) => {
    let term = req.query.term.toLowerCase();
    res.send(bien(term));
})
function bien(x) {
    return (x === 'life' ||x === 'success' || x === 'miss') ? x : "";
}

app.listen(port, () => {
    console.log("Your app is listening on port:" + port);
});