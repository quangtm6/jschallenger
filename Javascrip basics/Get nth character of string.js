// Write a function that takes a string (a) and a number (n) as argument
// Return the nth character of 'a'
function myFunction(a, n) {

    return console.log(a[n - 1]);
}

myFunction('abcd', 1);
myFunction('zyxbwpl', 5);
myFunction('gfedcba', 3);