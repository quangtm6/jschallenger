// Write a function that takes a value as argument
// Return the type of the value
function myFunction(a) {

    return console.log(typeof (a));
}

myFunction(1);
myFunction(false);
myFunction({});
myFunction(null);
myFunction('string');
myFunction(['array']);