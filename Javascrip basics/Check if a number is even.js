// Write a function that takes a number as argument
// If the number is even, return true
// Otherwise, return false
function myFunction(a) {

    return console.log(a % 2 === 0);
}

myFunction(10);
myFunction(-4);
myFunction(5);
myFunction(-111);