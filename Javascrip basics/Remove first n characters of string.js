// Write a function that takes a string (a) as argument
// Remove the first 3 characters of a
// Return the result
function myFunction(a) {

    return console.log(a.slice(3));
}

myFunction('abcdefg');
myFunction('1234');
myFunction('fgedcba');