// It seems like something happened to these strings
// Can you figure out how to clear up the chaos?
// Write a function that joins these strings together such that they form the following words:
// 'Javascript', 'Countryside', and 'Downtown'
// You might want to apply basic JS string methods such as replace(), split(), slice() etc
function myFunction(a, b) {
    a = a.replace('%', '');
    b = b.replace('%', '');
    let x = '';
    for(let i = b.length -1; i >= 0; i--) {
        x += b[i];
    }
    return console.log(a.charAt(0).toUpperCase() + a.slice(1) + x);
}
//function myFunction(a, b) {
//   const func = x => x.replace('%','');
//   const first = func(a);
//   const second = func(b).split('').reverse().join('');
//   return first.charAt(0).toUpperCase() + first.slice(1) + second;
// }
myFunction('java', 'tpi%rcs')
myFunction('c%ountry', 'edis')
myFunction('down', 'nw%ot')