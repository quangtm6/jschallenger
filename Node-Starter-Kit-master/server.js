const express = require('express');
const app = express();
const port = 3000;

app.get("/", (req, res) => {
    let searchQuery = req.query.search;
    res.send("Yay Node! You searched for " + searchQuery);
    console.log(req);
});

app.get("/chocolate", (req, res) => {
    let amount = req.query.amount;
    res.send(`Mm ${amount} chocolates: 0`);
});

app.get("/codeyourfuture", (req,res) => {
    res.send("Code your future");
})

app.listen(port , () => {
    console.log(`Server is listening on port ${port}. Ready to accept requests!`);
    console.log();
});
