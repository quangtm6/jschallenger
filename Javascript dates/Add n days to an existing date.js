// Write a function that takes as argument a date instance (a) and a number (b)
// It should add b days to a and return the number of milliseconds since January 1, 1970, 00:00:00 UTC
function myFunction(a, b) {
    a.setDate(a.getDate() + b);
    return console.log(a.valueOf());
}
//function myFunction(a, b) {
//   const currentDays = a.getDate();
//   return a.setDate(currentDays + b)
// }
myFunction(new Date(Date.UTC(2000,1,1)), 31);
myFunction(new Date(Date.UTC(2000,1,1)), 10);
myFunction(new Date(Date.UTC(2000,2,28,)), 2);