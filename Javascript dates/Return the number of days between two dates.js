// Write a function that takes two date instances as argument
// It should return the number of days that lies between those dates
function myFunction(a, b) {
    diff = Math.abs(b.getTime() - a.getTime());
    Total = Math.ceil(diff/ (1000 * 60 * 60 * 24));
    return console.log(Total);
}
//function myFunction(a, b) {
//    const dif = Math.abs(a - b);
//    return dif / 1000 / 60 / 60 / 24
// }
myFunction(new Date('2020-06-11'), new Date('2020-06-01'));
myFunction(new Date('2000-01-01'), new Date('2020-06-01'));