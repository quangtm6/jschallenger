// Write a function that takes a Set as argument
// Convert the Set to an Array
// Return the Array
function myFunction(set) {

    return console.log(Array.from(set));
}
//function myFunction(set) {
//   return [...set];
// }
myFunction(new Set([1, 2, 3]));
myFunction(new Set([123]));
myFunction(new Set(['1', '2', '3']));
myFunction(new Set('123'));