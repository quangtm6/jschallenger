// Write a function that takes a Set and an array as arguments
// If not already existing, add each element in the array to the Set
// Return the modified Set
function myFunction(set, arr) {
    const newSet = new Set([...set, ...arr]);
    return console.log(newSet);
}
//function myFunction(set, arr) {
//   arr.forEach((e) => set.add(e));
//   return set;
// }
myFunction(new Set([1, 2, 3]), [4, 5, 6]);
myFunction(new Set('12345'), [...'6789']);
myFunction(new Set([1, 2, 3]), [2, 3]);