const express = require('express');
const app = express();
const fs = require('fs');

const  messages = require('./messages.json');

app.use(express.urlencoded({extended:true}));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/messages', (req, res) => {
     res.json(messages);
});

app.get('/messages/:id', (req, res) => {
    const inputID = req.params.id;
    const message = messages.filter(ele => ele.id == inputID);
    res.json(message);
})

app.get('/messages/latest', (req, res) => {
    res.json(messages.slice(-10));
    console.log(messages.slice(-10));
});

app.get('/messages/search', (req, res) => {
    const texts = req.query.text;
    const mes = messages.filter(ele => {return ele.text.toLowerCase().indexOf(texts.toLowerCase()) !== -1});
    res.json(mes);
})

app.post('/', (req,res) => {
     if(req.body.from && req.body.text) {
         let ID = 0;
         if(messages.length > 0) {ID = messages[messages.length -1].id + 1}
         req.body.id = ID;
         console.log(req.body);
         messages.push(req.body);
         res.sendStatus(200);
         fs.writeFile("messages.json", JSON.stringify(messages), err => {
             if (err) throw err;
             console.log('The file há been saved!')
         });
     } else{res.sendStatus(400)}
})


app.listen(3000);