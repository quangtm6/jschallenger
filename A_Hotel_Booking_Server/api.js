const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const BookingsModel = require('./BookingsSchema');
const Joi = require("joi");
const fs = require("fs");
const moment = require("moment");

const query = 'mongodb://localhost:27017/Bookings'
const db = (query);
mongoose.Promise = global.Promise;

mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true}, (error) => {
    if (error) {
        console.error(error)
    }
});

router.get('/create', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

router.post('/create', async (req, res) => {
    console.log(req.body)
    const schema = Joi.object({
        title: Joi.string().required(),
        firstName: Joi.string().required(),
        surName: Joi.string().required(),
        email: Joi.string().email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}}).required(),
        roomId: Joi.number().integer().required(),
        checkInDate: Joi.date().required().greater(Date.now() + 24 * 60 * 60 * 1000),
        checkOutDate: Joi.date().required().greater(Joi.ref('checkInDate')),
        id: Joi.number().integer()
    })
    try {
       const data = await schema.validateAsync(req.body)
        const newBookings = new BookingsModel(data)
        await newBookings.save(data)
        console.log('The file been saved');
        res.redirect('/create');
    } catch (err) {
        console.error(err)
        res.sendStatus(400)
    }
})

router.get('/bookings', async (req, res) => {
    const data = await BookingsModel.find()
    res.send(data)
})

router.get('/bookings/search', async (req, res) => {
    const date = req.query.date;
    const term = req.query.term;
    if (date) {
        const dataDate = await BookingsModel.find({
            checkInDate: {$lte: new Date(date)},
            checkOutDate: {$gt: new Date(date)}
        })
        res.send(dataDate)
    } else if (term) {
        const dataTerm = await BookingsModel.find({
            $or: [{email : {$in: term}},
                {firstName: {$in : term}},
                {surName: {$in: term}}
            ]
        })
        res.send(dataTerm);
    } else { res.sendStatus(400) }
})

router.get('/bookings/:id', async (req, res) => {
    const inputID = req.params.id;
    if (inputID) {
        let data = await BookingsModel.findOne({id: inputID})
        res.send(data)
    } else {
        res.sendStatus(404)
    }
})

router.delete('/bookings/:id', async (req, res) => {
    const inputID = req.params.id;
    if (inputID) {
        let data = await BookingsModel.deleteOne({id: inputID})
        res.send(data)
    } else {
        res.sendStatus(404)
        }
})
module.exports = router;








