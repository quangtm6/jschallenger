const mongoose = require("mongoose");
const BookingsSchema = new mongoose.Schema({
    title: String,
    firstName: String,
    surName: String,
    email: String,
    roomId: Number,
    checkInDate: Date,
    checkOutDate: Date
})
module.exports = mongoose.model('bookings', BookingsSchema, 'Bookings');