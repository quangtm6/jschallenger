const express = require('express');
const fs = require('fs');
const moment = require('moment');
const Joi = require('joi');
const ev = require('email-validator')

const bookings = require('./bookings');
const app = express();

app.use(express.urlencoded({extended:true}))
app.get('/create', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

app.post('/create', async (req, res) => {
    console.log(req.body)
    const schema = Joi.object({
        title: Joi.string().required(),
        firstName: Joi.string().required(),
        surName : Joi.string().required(),
        email: Joi.string().email({minDomainSegments: 2, tlds:{allow:['com','net']}}).required(),
        roomId: Joi.number().integer().required(),
        checkInDate: Joi.date().required().greater(Date.now() + 24 * 60 * 60 * 1000),
        checkOutDate: Joi.date().required().greater(Joi.ref('checkInDate')),
        id: Joi.number().integer().required()
    })
    try {
        await schema.validateAsync(req.body)
        let ID = 1;
        if (bookings.length > 0) {
            ID = bookings[bookings.length - 1].id + 1
        }
        req.body.id = ID;
        bookings.push(req.body);
        res.sendStatus(200);
        fs.writeFileSync('bookings.json', JSON.stringify(bookings), err => {
            if (err) throw err;
            console.log('The file been saved');
        })
    }
    catch(err){console.error(err)
        res.sendStatus(400)}
})

app.get('/bookings', (req, res) => {
    res.json(bookings);
})

app.get('/bookings/search', (req, res) => {
    const date = req.query.date;
    const term = req.query.term;
    if (date) {
        const booking = bookings.filter(ele => {
            console.log(moment(ele.checkInDate).format('YYYY-MM-DD') <= moment(date).format('YYYY-MM-DD'))
            console.log(moment(ele.checkOutDate).format('YYYY-MM-DD') > moment(date).format('YYYY-MM-DD'))

            return moment(ele.checkInDate).format('YYYY-MM-DD') <= moment(date).format('YYYY-MM-DD')
            && moment(ele.checkOutDate).format('YYYY-MM-DD') > moment(date).format('YYYY-MM-DD');
        });
        res.json(booking);
    } else if (term) {
        const booking = bookings.filter(ele => ele.email.includes(term)|| ele.firstName.includes(term)
            || ele.surName.includes(term) );
        res.json(booking);
    } else {res.sendStatus(400)}
})

app.get('/bookings/:id', (req, res) => {
    const inputID = req.params.id;
    if(inputID){
        const booking = bookings.filter(ele => ele.id == inputID);
        res.json(booking);
    } else {res.json(404)}
})

app.delete('/bookings/:id', (req, res) => {
    if(inputID){
        const booking = bookings.filter(ele => ele.id == inputID);
        delete(res.json(booking));
    }else {res.json(404)}
})
app.listen(3000,() => {
    console.log()
})