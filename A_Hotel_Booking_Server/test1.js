const express = require('express');
const fs = require('fs');
const moment = require('moment');
const Joi = require('joi');
//const bookings = require('./bookings');
const mongoose = require('mongoose')
//const {date} = require("joi");
const app = express();
const Port = process.env.PORT || 3000;
const api = require('./api')

app.use(express.urlencoded({extended:true}));
app.use(express.json());
// app.use('/api.', api);
app.use(api);

app.listen(Port,() => {
    console.log('Server is listening at port:' + Port)
})