const express = require('express');
const mg = require('mongoose');
const mongoDB = 'mongodb://localhost:27017';

const Schema = mg.Schema;
const SomeModelSchema = new Schema({
    name: String,
    binary: Buffer,
    living: Boolean,
    update: {type: Date, default: Date.now()},
    age: {type: Number, min: 18, max: 65, require: true},
    mixed: Schema.Types.Mixed,
    _someId: Schema.Types.ObjectId,
    array: [],
    ofString: [String],
    nested: {stuff: {type: String, lowercase: true, trim: true}}
})
const SomeSchema = mg.model('SomeModel', SomeModelSchema)
mg.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true}).then(client => {
    console.log('connected to Database')
}).catch(error => console.error(error))
const db = mg.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

