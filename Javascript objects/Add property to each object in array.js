// Write a function that takes an array of objects and a string as arguments
// Add a property with key 'continent' and value equal to the string to each of the objects
// Return the new array of objects
// Tipp: try not to mutate the original array
function myFunction(arr, str) {
    for(let i = 0; i < arr.length; i++){
        arr[i].continent = str;
    }
    return console.log(arr);
}
//function myFunction(arr, str) {
//   return arr.map((obj) => ({ ...obj, continent: str }));
// }
myFunction([{ city: 'Tokyo', country: 'Japan' }, { city: 'Bangkok', country: 'Thailand' }], 'Asia');
myFunction([{ city: 'Stockholm', country: 'Sweden' }, { city: 'Paris', country: 'France' }], 'Europe');