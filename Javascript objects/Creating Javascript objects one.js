// Write a function that takes a string as argument
// Create an object that has a property with key 'key' and a value equal to the string
// Return the object
function myFunction(a) {

    return console.log({key: a});
}

myFunction('a');
myFunction('z');
myFunction('b');