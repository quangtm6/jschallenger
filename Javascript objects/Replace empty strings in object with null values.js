// Write a function that takes an object as argument
// Some of the property values contain empty strings
// Replace empty strings and strings that contain only whitespace with null values
// Return the resulting object
function myFunction(obj) {
    Object.keys(obj).forEach(key => {
        if (obj[key].trim() === '') {
            obj[key] = null;
        }
    });
    return console.log(obj);
}
myFunction({ a: 'a', b: 'b', c: '' });
myFunction({ a: '', b: 'b', c: ' ', d: 'd' });
myFunction({ a: 'a', b: 'b ', c: ' ', d: '' });