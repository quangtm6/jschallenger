// Write a function that takes an array of numbers as argument
// Convert the array to an object
// It should have a key for each unique value of the array
// The corresponding object value should be the number of times the key occurs within the array
function myFunction(a) {
    let obj = {};
    a.forEach((ele) => {obj[ele] = a.filter(val => val === ele ).length});

    return console.log(obj);
}
//function myFunction(a) {
//    return a.reduce((acc, cur) => {
//    	return { ...acc, [cur]: (acc[cur] || 0) + 1 };
//    }, {});
// }
myFunction([1,2,2,3]);
myFunction([9,9,9,99]);
myFunction([4,3,2,1]);