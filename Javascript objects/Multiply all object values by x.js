// Write a function that takes an object (a) and a number (b) as arguments
// Multiply all values of 'a' by 'b'
// Return the resulting object
function myFunction(a, b) {
    let x = Object.values(a).map(e => e * b);
    let y = Object.keys(a);
    let obj = {};
    y.forEach((ele, index) => obj[ele] = x[index]);
    return console.log(obj);
}
// function myFunction(a, b) {
//    return Object.entries(a).reduce((acc, [key, val]) => {
//    	return { ...acc, [key]: val * b };
//    }, {});
// }
myFunction({a:1,b:2,c:3},3);
myFunction({j:9,i:2,x:3,z:4},10);
myFunction({w:15,x:22,y:13},6);