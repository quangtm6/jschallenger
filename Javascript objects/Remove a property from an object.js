// Write a function that takes an object as argument
// It should return an object with all original object properties
// except for the property with key 'b'
function myFunction(obj) {
    delete obj.b;
    return console.log((obj));
}
//function myFunction(obj) {
//   const { b, ...rest } = obj;
//   return rest;
// }
myFunction({ a: 1, b: 7, c: 3 })
myFunction({ b: 0, a: 7, d: 8 })
myFunction({ e: 6, f: 4, b: 5, a: 3 })