// Write a function that takes an object as argument containing properties with personal information
// Extract firstName, lastName, size, and weight if available
// If size or weight is given transform the value to a string
// Attach the unit cm to the size
// Attach the unit kg to the weight
// Return a new object with all available properties that we are interested in
function myFunction(obj) {
    let newObj = {...obj};
    Object.keys(newObj).forEach((key) => {
        if (key === 'age' || key === 'email') {
            delete newObj[key]
        }
        if (key === 'size') {
            newObj[key] += 'cm';
        }
        if (key === 'weight') {
            newObj[key] += 'kg';
        }
    })
    return console.log(newObj);
}
// function myFunction(obj) {
//   return {
//     fn: obj.fn,
//     ln: obj.ln,
//     ...(obj.size && { size: `${obj.size}cm` }),
//     ...(obj.weight && { weight: `${obj.weight}kg` }),
//   };
// }
myFunction({fn: 'Lisa', ln: 'Müller', age: 17, size: 175, weight: 67});
myFunction({fn: 'Martin', ln: 'Harper', age: 26, email: 'martin.harper@test.de', weight: 102});
myFunction({fn: 'Andrew', ln: 'Harper', age: 81, size: 175, weight: 71});
myFunction({fn: 'Matthew', ln: 'Müller', age: 19, email: 'matthew@mueller.de'});