// Write a function that takes an object as argument
// Somehow, the properties and keys of the object got mixed up
// Swap the Javascript object's key with its values and return the resulting object
function myFunction(obj) {
    let x = Object.values(obj);
    let y = Object.keys(obj);
    let Obj = {}
    x.forEach((ele, index) => Obj[ele] = y[index]);
    return console.log(Obj);
}
//function myFunction(obj) {
//    return Object.entries(obj).reduce((acc, [key, val]) => {
//       return { ...acc, [val]: key };
//    }, {});
// }
myFunction({z:'a',y:'b',x:'c',w:'d'});
myFunction({2:'a',4:'b',6:'c',8:'d'});
myFunction({a:1,z:24});