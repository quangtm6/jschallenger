// Write a function that takes two arrays (a and b) as arguments
// Create an object that has properties with keys 'a' and corresponding values 'b'
// Return the object
function myFunction(a, b) {
    let obj = {};
    a.forEach((ele, index) => obj[ele] = b[index]);
    return console.log(obj)
}
// function myFunction(a, b) {
//    return a.reduce((acc, cur, i) => ({ ...acc, [cur]: b[i] }), {});
// }
myFunction(['a','b','c'],[1,2,3]);
myFunction(['w','x','y','z'],[10,9,5,2]);
myFunction([1,'b'],['a',2]);
